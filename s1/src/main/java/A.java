public class A extends class B{
}

public class B{
    public String param;
    public E e;
    public D d;

    public void x(){};

    public void y(){};
}

public class U{
    public void z(B b);
}

public class C{
    public B b = new B();
    public C(B b){
        this.b=b;
    }
}
public class D{}

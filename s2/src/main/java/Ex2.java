import jdk.internal.logger.BootstrapLogger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class Ex2 implements ActionListener {
    private JButton button;
    private JFrame frame;
    private JTextField text;
    private JLabel label;

    public Ex2() {
        makeGUI();
    }

    public void makeGUI() {
        frame = new JFrame("");
        frame.setLayout(null);
        frame.setLocation(700, 150);

        arrangeComponents();

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(390, 320);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    public void arrangeComponents() {
        label = new JLabel("Insert text here");
        label.setBounds(20, 20, 130, 50);
        label.setVisible(true);
        frame.add(label);

        text = new JTextField("");
        text.setBounds(160, 20, 200, 50);
        text.setVisible(true);
        frame.add(text);

        button = new JButton("Print text");
        button.setBounds(20, 200, 150, 40);
        button.setVisible(true);
        button.addActionListener(this);
        frame.add(button);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == button) {

            System.out.println(text.getText());
        }
    }

    public static void main(String[] args) {
        new Ex2();
    }
}
